# ZUM Digiscreen

ZUM Digiscreen ist ein interaktiver Bildschirm für den Präsenz- oder Fernunterricht im Klassenzimmer. 

Die Anwendung wird unter der GNU GPLv3-Lizenz veröffentlicht.
Außer den Schriftarten Abril Fat Face, Orbitron und Material Icons (Apache License Version 2.0) 
und der Schriftart HKGrotesk (Sil Open Font License 1.1)

Les symboles pictographiques utilisés dans le module Histoire sont la propriété du Gouvernement d'Aragon et ont été créés par Sergio Palao pour ARASAAC (https://arasaac.org), qui les distribuent sous Licence Creative Commons BY-NC-SA 4.0.

Les émojis utilisés dans le module Rétroaction proviennent du projet OpenMoji (https://openmoji.org) et sont publiés sous Licence Creative Commons BY-SA 4.0.

Les sons utilisés dans le module Compte à rebours proviennent de la Sonothèque (https://lasonotheque.org) et sont publiés dans le domaine public.

### Vorbereitung
```
npm install
```

### Development Server starten
```
npm run dev
```

### Compilation et minification des fichiers
```
npm run build
```

### Variables d'environnement (fichier .env à créer à la racine avant compilation)
```
AUTHORIZED_DOMAINS (liste des domaines autorisés pour les requêtes POST et l'API, séparés par une virgule / * par défaut)
VITE_GOOGLE_API_KEY= (Google API-Schlüssel für Youtube)
VITE_PIXABAY_API_KEY= (für Pixabay  API)
VITE_PHPFPM_HOST=127.0.0.127 (PHP FPM host for the PHP backend)
VITE_PHPFPM_PORT=9000 (PHP FPM PORT for the PHP backend)
```

Wenn VITE_PHPFPM_HOST oder VITE_PHPFPM_PORT nicht gesetzt sind, werden die
Funktionen die ein Backend benötgen ausgeschaltet.

### Kompilieren und Minimieren
```
npm run build
```

### Backend Server für lokale Entwicklung
```
php -S 127.0.0.1:8000 (pour le développement uniquement)
```

### Produktivbetrieb
Der dist-Ordner kann direkt auf einem Dateiserver bereitgestellt werden. Diese kompilierte Version enthält nicht die Pixabay- und Google-API-Schlüssel für YouTube.

### Demo
https://digiscreen.zum.de

### Danksagungen

ZUM Digiscreen ist eine modifizierte Version des Digiscreen von Emmanuel ZIMMERT https://ladigitale.dev/digiscreen

Italienische Übersetzung von Paolo Mauri (ttps://gitlab.com/maupao) und Roberto Marcolin (nilocram)

Übersetzung ins Spanische von Fernando S. Delgado Trujillo (https://gitlab.com/fersdt)

Niederländische Übersetzung von Erik Devlies

Deutsche Übersetzung bei einer Aktion im "Twitter-Lehrerzimmer" [#twitterlehrerzimmer](https://twitter.com/search?q=%23twitterlehrerzimmer) / [#twlz](https://twitter.com/search?q=%23twlz) von [@uivens](https://twitter.com/uivens) (Ulrich Ivens), [@eBildungslabor](https://twitter.com/eBildungslabor) (Nele Hirsch) und [@teachitalizer](https://twitter.com/teachitalizer) (Holger Skarba)

Übersetzung ins Kroatische von Ksenija Lekić (https://gitlab.com/Ksenija66L)


### Spenden
Bitte richtet eure Spenden an das Ursprungsprojekt

Open Collective : https://opencollective.com/ladigitale

Liberapay : https://liberapay.com/ladigitale/


