{
  inputs.flake-utils = {
    url = "github:numtide/flake-utils";
    inputs.nixpkgs.follows = "nixpkgs";
  };

  outputs = { self, nixpkgs, flake-utils }: 
  flake-utils.lib.eachDefaultSystem (system:
   let pkgs = nixpkgs.legacyPackages.${system};
   in {
      devShell = pkgs.mkShell {
        nativeBuildInputs = [ pkgs.nodejs-16_x ];
        shellHook = ''
mkdir -p .flake/bin
ln --force --symbolic "$(which node)" .flake/bin/
ln --force --symbolic "$(which npm)" .flake/bin/
ln --force --symbolic "$(which npx)" .flake/bin/
        '';
      };
    }
  );
}

