FROM node:18-alpine as build-stage
WORKDIR /app
COPY package*.json ./
RUN npm clean-install
COPY ./ .
RUN npm run build:dist

FROM php:8-fpm-alpine as production-php
RUN mkdir /inc
COPY --from=build-stage /app/inc /inc

FROM nginx:alpine as production-web
RUN mkdir /app
ENV NGINX_ENVSUBST_FILTER="VITE_*"

# provide defaults for SSI Placeholders in zum-digiscreen.conf.template (otherwise the config becomes invalid)
ENV VITE_GOOGLE_API_KEY=""
ENV VITE_PIXABAY_API_KEY=""
# TODO rename the NGINX_ENVSUBST_FILTER and avoid the VITE_ prefix
ENV VITE_PHPFPM_HOST="127.0.0.127"
ENV VITE_PHPFPM_PORT="9000"

# for consistency in the Dockerfile. Will be reset on nginx entrypoint by /docker-entrypoint.d/15-vite-phpfm-unset-toggle.envsh
ENV VITE_PHPFPM_UNSET_TOGGLE=""
COPY --from=build-stage /app/dist /app
# must match production-php location for inc
COPY --from=build-stage /app/inc /inc
# use default.conf.template name to override default
COPY nginx.conf.template /etc/nginx/templates/default.conf.template
COPY --chmod=0544 15-vite-phpfm-backend-present.envsh /docker-entrypoint.d/
COPY --chmod=0544 15-vite-phpfm-unset-toggle.envsh /docker-entrypoint.d/
