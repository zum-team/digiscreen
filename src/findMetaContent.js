export const findMetaContent = (name) => {
  const el = document.querySelector(`meta[name=${name}]`);
  return (el && el.getAttribute("content")) || null;
};
